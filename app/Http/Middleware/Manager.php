<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class Manager
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check()) {
            return redirect('users/login');
        } else {
            // dd(Auth::user());
            $user =  User::find(Auth::user()->id); 
            // Auth::user();
            if($user->hasRole('manager')){
                return $next($request);
            } else return redirect('/');
        }
    }
}
