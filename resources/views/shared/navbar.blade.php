<nav class="navbar navbar-expand-md">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{!! action('PagesController@home') !!}">Learning Laravel</a>
        </div>
        <!-- Navbar Right -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="{!! action('PagesController@home') !!}">Home</a></li>
                <li><a href="{!! action('BlogController@index') !!}">Blog</a></li>
                <li><a href="{!! action('TicketsController@index') !!}">Tickets</a></li>
                <li><a href="{!! action('TicketsController@create') !!}">Create</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                        aria-expanded="false">Member
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        @if (Auth::check())
                        @role ('manager')
                            <li><a href="{!! action('PagesController@home') !!}">Admin</a></li>
                        @endrole
                            <li><a href=" {!! action('Auth\LoginController@logout') !!}">Logout</a></li>
                        @else
                        <li><a href="{!! action('Auth\RegisterController@showRegistrationForm') !!}">Register</a></li>
                        <li><a href="{!! action('Auth\LoginController@showLoginForm') !!}">Login</a></li>
                        @endif
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>